const sitemap = require('nextjs-sitemap-generator');  

sitemap({  
    baseUrl: 'https://seocarlist.netlify.app',  
    pagesDirectory: __dirname + "/pages",  
    targetDirectory : 'static/'  
  });
module.exports = {
    serverRuntimeConfig: {
        PROJECT_ROOT: __dirname
    }
}