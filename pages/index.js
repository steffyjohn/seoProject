import Head from 'next/head'
import styles from '../styles/Home.module.css';
import Header from '../components/Header';
import Footer from '../components/Footer';
import ProductList from '../components/ProductList';

 function Home(props) {
  return (
    <div className={styles.container}>
      <Head>
      <meta name="google-site-verification" content="af7wR-r6dekfnW0wOSTMCVreu-gbZ-_pM_ILsChllMQ" />
  <meta name="description" content="Buy beautiful, car toy."/>
  <title>Buy Beautiful Car Toy</title>
  <link rel="stylesheet" href="https://cdn.snipcart.com/themes/v3.0.0-beta.3/default/snipcart.css" />
</Head>

      <Header />
      <main className="main">
        <ProductList products={props.products} />
      </main>
      <Footer />
    </div>
  )
}

Home.getInitialProps = async () => {
  return {
    products: [
      { id: "nextjs-seo_car1", name: "GetBest", price: 25.0, image: "static/car1.jpg", description: "GetBest Twinink 909 Kids 12V Battery Operated Ride on Jeep with Music, Lights, Spring Suspension, Swing Motion and Remote Control, Blue" },
      { id: "nextjs-seo_car2", name: "Ferrari", price: 25.00, image: "static/car2.jpg", description: "Your feet will enjoy this fluffy car. Well... that is until it gets dirty!Remote Car Ferrari FF, 1:24 Scale Officially Licensed Model" },
    ]
  }
}

export default Home